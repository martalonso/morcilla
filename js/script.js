$(document).ready(function() {
		init();
});
	
var content= document.getElementById('content');
	function init(){

 	var search = document.getElementById('search');		
	$('#search').keypress(function(e) {
		var data = $('#search').serialize();
		data+='&action=search';	   
	  if(e.which == 13) {	  	
	  	//console.log(data);
	  		$.ajax({
				type: 'GET',
				url : 'functions.php',
				dataType : 'json',
				data: data,
				success:function(response){
					console.log('search');
					$('#content').html(response.output);
					actions();
		
				}, 
				error: function( data, textStatus, jqXHR){
					console.log(textStatus);
					$('#content').html('<p> Error search </p>');
				}

			});
			return false;
   		}
	});
	
}	// END init()


function actions(){	

	// NEW DATA 	$_GET['action'] === 'add-data'
	$("#newdata").submit(function() {
		//console.log('new data');
		var data = $(this).serialize();
		data += '&action=add-data';
			$.ajax({
		    type: 'GET',
		    url: 'functions.php',
		    dataType : 'json',
		    data: data,
		    success: function(response) {
		   		console.log('new data ok');
	         $('#content').html(response.output);
	         actions();
		    },
		    error : function(data, textStatus, jqXHR){
				console.log(textStatus);
				$('#content').html('<p> Error add data </p>');
			}
	    });
	    return false;
	});


	// EDIT DATA PAGE	$_GET['action'] === 'edit'
	$(".editlink").click(function(e) {
	 	var name = $('#name').text();
	 	//console.log(name);
	 	var row = $(this).parent();
	 	var data = new Array();

	 	data.push(name);

		$("td:lt(4)").each(function () {
		    col = $(this).text();
		    data.push(col);
		});

		console.log(data);
    	$.ajax({
		    type: 'GET',
		    url: 'functions.php',
		    dataType : 'json',
		    //data: 'edit='+type+'&action=edit',
		    data: 'edit='+data+'&action=edit',
		    success: function(response) {
		    	//console.log('editar');
	         $('#content').html(response.output);	
	          actions();	         
		    },
		    error : function(data, textStatus, jqXHR){
						console.log(textStatus);
						$('#content').html('<p> Error edit data</p>');
					}
	    });
	    return false;
    });

	// EDIT DATA FORM	$_GET['action'] === 'edit-data'
	$("#editdata").submit(function(e) {	 
		 	var data = $(this).serialize();
		 	data += '&action=edit-data';
		 	console.log(data);
		 
     		$.ajax({
			    type: 'GET',
			    url: 'functions.php',
			    dataType : 'json',				    
			    data: data,
			    success: function(response) {
			    		console.log('editado');
			    		 $('#content').html(response.output);	
			    		  actions();	
			    },
			    error : function(data, textStatus, jqXHR){
							console.log(textStatus);
							$('#content').html('<p> Error al editar data</p>');
						}
		    });
		    return false;
    });

	// DELETE DATA	$_GET['action'] === 'delete'
	$(".deletelink").click(function(event) {
	 	var name = $('#name').text();
	 	var row = $(this).parent().parent();
	 	var data = new Array();

	 	data.push(name);

		$("td:lt(4)").each(function () {
		    col = $(this).text();
		    data.push(col);
		});


    	$.ajax({
		    type: 'GET',
		    url: 'functions.php',
		    dataType : 'json',
		    data: 'delete='+data+'&action=delete',
		    success: function(response) {
	         $('#content').html(response.output);	
	          actions();	         
		    },
		    error : function(data, textStatus, jqXHR){
				console.log(textStatus);
				$('#content').html('<p> Error delete data</p>');
			}
	    });
	    return false;
    });

	// EDIT DATA FORM	$_GET['action'] === 'edit-data'
	$("#deleteData").submit(function(e) {	
	 	var data = $(this).serialize();
	 	data += '&action=delete-data';
			$.ajax({
		    	type: 'GET',
		    	url: 'functions.php',
		   		dataType : 'json',				    
		    	data: data,
		    	success: function(response) {
		    		$('#content').html(response.output);	
		    		actions();	
		    	},
		    	error : function(data, textStatus, jqXHR){
					$('#content').html('<p> Error al insertar cliente </p>');
				}
	    });
		    return false;
    });

	 // ADD CLIENT	 $_GET['action'] === 'add-client'
	 $("#clientform").submit(function() {
	 	var data = $(this).serialize();
	 	data += '&action=add-client';
 			$.ajax({
			    type: 'GET',
			    url: 'functions.php',
			    dataType : 'json',				    
			    data: data,
			    success: function(response) {
			    	console.log('aaaaaaaaaaaa');
			    		$('#content').html(response.output);
			    		 actions();
			    },
			    error : function(data, textStatus, jqXHR){
							console.log(textStatus);
							$('#content').html('<p> Error al insertar cliente </p>');
						}
		    });
		    return false;
	});

};

